# Red Google+ Theme
## 概要(Overview)
Google+のStylish,Stylus向けのテーマです。Google+の赤色のトップバーを復元します。
拡張機能のインストールが必要になります。現時点ではChrome,Firefox以外のブラウザ、Stylish,Stylus以外の拡張機能はサポート外となります。  

**拡張機能(Stylus)**:  
[![Chrome Web Store](res/Chrome.png "Chrome extension")](https://chrome.google.com/webstore/detail/stylus/clngdbkpkpeebahjckkjfobafhncgmne)  [![Add-ons for Firefox](res/Firefox.png "Firefox extension")](https://addons.mozilla.org/ja/firefox/addon/styl-us/)
  
**テーマ本体**:  
[![Red Google+ Theme](res/Theme.png "Theme")](https://userstyles.org/styles/163296/red-google-plus-theme)
  

## 開発・貢献(Contribution)
開発・貢献は大歓迎です。Pull Requestはもちろん、issueもお待ちしております。  

## ライセンス(License)
CC BY-NC-SA 4.0  